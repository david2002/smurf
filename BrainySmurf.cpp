#include "BrainySmurf.h"



BrainySmurf::BrainySmurf(std::string Name, std::string Type)
{
	_type = Type;
	_name = Name;
	_lifePoints = 30;
	_magicPoints = 1;
}


BrainySmurf::~BrainySmurf()
{
}

/*
The function makes magic:
Adds 10 life points to all the smurfs
Except for Hadards who performed the magic
The function also removes one magic point
*/
void BrainySmurf::MakeMagic(std::vector<Smurfs>& allSmurfs,int& GargamelLives, int i)
{
	for (int j = 0;j < allSmurfs.size();j++)
	{
		if (j != i)
		{
			allSmurfs[j]._lifePoints += 10;
		}
	}
	allSmurfs[i]._magicPoints--;

	std::cout << allSmurfs[i]._name << "Told A Smart Joke, everybody now have +10 life points" << std::endl;
}

/*
Feed Function:
Adds 4 points of life and 1 magic points
*/
void BrainySmurf::feed(std::vector<Smurfs>& allSmurfs,int i)
{
	allSmurfs[i]._lifePoints += 4;
	allSmurfs[i]._magicPoints += 1;
}

