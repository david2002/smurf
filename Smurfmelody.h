#pragma once
#include"Smurfs.h"
#include<string>
#include <string.h>
#include <iostream>
#include<vector>

class Smurfmelody:public Smurfs
{
public:
	Smurfmelody(std::string Name, std::string Type);
	~Smurfmelody();
	virtual void MakeMagic(std::vector<Smurfs>& allSmurfs,int& GargamelLives, int i );
	virtual void feed(std::vector<Smurfs>& allSmurfs,int i);
};

