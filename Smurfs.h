#pragma once
#include<string>
#include <string.h>
#include <iostream>
#include<vector>

class Smurfs
{
public:
	Smurfs() {/**/ };
	Smurfs(std::string Name, std::string Type) {/**/ };
	~Smurfs() {/**/ };
	virtual void MakeMagic(std::vector<Smurfs>& allSmurfs,int& GargamelLives, int i){/**/ };
	std::ostream& operator<<(bool val) {/**/ };
	virtual void feed(std::vector<Smurfs>& allSmurfs, int i){/**/ };
//protected:
	std::string _name;
	std::string _type;
	std::string _masterName;
	int _lifePoints;
	int _magicPoints;
	//Smurfs(void);
};

