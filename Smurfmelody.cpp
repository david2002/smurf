#include "Smurfmelody.h"
#include<vector>

Smurfmelody::Smurfmelody(std::string Name, std::string Type)
{
	_type = Type;
	_name = Name;
	_lifePoints = 30;
	_magicPoints = 1;
}


Smurfmelody::~Smurfmelody()
{
}
/*
The function makes magic:
Adds a magic point to all the smurfs
So the Dredds who made it do not lose any magic point
But she drops him 10 points of life
*/
void Smurfmelody::MakeMagic(std::vector<Smurfs>& allSmurfs,int& GargamelLives, int i)
{
	_magicPoints--;
	for (int j = 0;j < allSmurfs.size();j++)
	{
		allSmurfs[j]._magicPoints++;
	}
	
	_lifePoints -= 10;
	std::cout << _name;
	std::cout << "made magic, She now has";
	std::cout << _lifePoints;
	std::cout << "life points";
}
/*
Feed Function:
Adds 2 points of life and 2 magic points
*/
void Smurfmelody::feed(std::vector<Smurfs>& allSmurfs,int i)
{
	allSmurfs[i]._lifePoints += 2;
	allSmurfs[i]._magicPoints += 2;
}
