#pragma once
#include"Smurfs.h"
#include<string>
#include <string.h>
#include <iostream>
#include<vector>
class BrainySmurf:public Smurfs
{
public:
	BrainySmurf(std::string Name, std::string Type);
	~BrainySmurf();
	virtual void MakeMagic(std::vector<Smurfs>& allSmurfs,int& GargamelLives , int i);
	virtual void feed(std::vector<Smurfs>& allSmurfs,int i);
};

