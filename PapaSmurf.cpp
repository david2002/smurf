#include "PapaSmurf.h"





PapaSmurf::PapaSmurf(std::string Name, std::string Type)
{
	_type = Type;
	_name = Name;
	_lifePoints = 30;
	_magicPoints = 1;
}


PapaSmurf::~PapaSmurf()
{
}

/*
The function makes magic:
Downloading Gregmel 10 points of life
But also brings down five points of life for Dardes
The function also removes one magic point
*/
void PapaSmurf::MakeMagic(std::vector<Smurfs>& allSmurfs,int& GargamelLives, int i)
{
	GargamelLives -= 10;
	allSmurfs[i]._lifePoints -= 5;
	allSmurfs[i]._magicPoints--;
	std::cout << allSmurfs[i]._name << "made magic on Gargamel,He now have" << allSmurfs[i]._lifePoints << "life point" << std::endl;
}

/*
Feed Function:
Adds 2 points of life and 5 magic points
*/
void PapaSmurf::feed(std::vector<Smurfs>& allSmurfs,int i)
{
	allSmurfs[i]._lifePoints += 2;
	allSmurfs[i]._magicPoints += 5;
}
