#pragma once
#include"Smurfs.h"
#include<string>
#include <string.h>
#include <iostream>
#include<vector>
#include"Smurfmelody.h"
#include"BrainySmurf.h"
#include"PapaSmurf.h"

class Game
{
public:
	Game();
	~Game();
	void Start();
	void printMenu();
	void switchCase(int choose);
	void CreateSmurfmelody();
	void CreateBrainySmurf();
	void CreatePapaSmurf();
	std::string CreateSmurfs(std::string Type);

	void feed();
	void fight();
	void GiveMoreAmount();
	void printAll();
	void printList();

	int GargamelLives;
private:
	std::string _masterName;
	std::vector<Smurfs> allSmurfs;	
};

