#include "Game.h"
#include<vector>


Game::Game()
{
	GargamelLives = 70;
}


Game::~Game()
{
}

/*
The function triggers the game
*/
void Game::Start()
{
	int choose = 1;
	std::cout << "What is your name master?";
	std::cin >> _masterName;
	std::cout<<std::endl;

	while (choose<8)
	{
		printMenu();
		do
		{
			std::cin >> choose;
		}while(choose <= 0 || choose > 9);
		switchCase(choose);
	}
	printMenu();

}

/*
The function prints the main menu
*/
void Game::printMenu()
{
	std::cout << std::endl;
	std::cout << "[0]-Display the menu again." << std::endl;
	std::cout <<"[1]-Create a Smurfmelody."<< std::endl;
	std::cout <<"[2]-Create an Brainy Smurf."<< std::endl;
	std::cout <<"[3]-Create an Papa Smurf."<< std::endl;
	std::cout <<"[4]-Feed."<< std::endl;
	std::cout <<"[5]-Fight."<< std::endl;
	std::cout <<"[6]-Give More Amount of uses."<< std::endl;
	std::cout <<"[7]-Print all details."<< std::endl;
	std::cout <<"[8]-Exit."<< std::endl;
	std::cout <<"What would you like to do master "<<_masterName<<"?"<< std::endl;
}

/*
The function calls the selected function
*/
void Game::switchCase(int choose)
{
	switch (choose)
	{
	case 0:
		break;
	case 1:
		CreateSmurfmelody();
		break;
	case 2:
		CreateBrainySmurf();
		break;
	case 3:
		CreatePapaSmurf();
		break;
	case 4:
		feed();
		break;
	case 5:
		fight();
		break;
	case 6:
		GiveMoreAmount();
		break;
	case 7:
		printList();
		break;
	case 8:
		std::cout << "GoodBye master " << _masterName << "! It was fun playing with you!" << std::endl;
		break;
	default:
		break;
	}



}

/*
The function creates a new Smurfmelody
*/
void Game::CreateSmurfmelody()
{
	std::string Type = "Smurfmelody";
	Smurfmelody _new(CreateSmurfs(Type),Type);
	allSmurfs.push_back(_new);
}

/*
The function creates a new BrainySmurf
*/
void Game::CreateBrainySmurf()
{
	std::string Type = "Brainy Smurf";
	BrainySmurf _new(CreateSmurfs(Type),Type);
	allSmurfs.push_back(_new);
}

/*
The function creates a new 
*/
void Game::CreatePapaSmurf()
{
	std::string Type = "Papa Smurf";
	PapaSmurf _new(CreateSmurfs(Type),Type);
	allSmurfs.push_back(_new);
}

/*
The function takes in details from the user to create a new smurf
*/
std::string Game::CreateSmurfs(std::string Type)
{
	int age;
	std::string name;
	std::cout << "Enter smurf name:";
	std::cin >> name;
	std::cout << std::endl;
	std::cout << "Enter smurf age:";
	std::cin >> age;
	std::cout << Type<<" " << name << "was born successfully!";
	return name;
}
/*
The function allows the user to choose a smurf
And operates the feeding function according to its type
*/
void Game::feed()
{
	int i = 0;
	printAll();
	do{
		std::cout << "Which smurf do you want to feed?" << std::endl;
		std::cin >> i;
	} while (i>allSmurfs.size()||i<0);
	
	if (allSmurfs[i]._type == "Smurfmelody")
	{
		allSmurfs[i].feed(allSmurfs,i);
	}
	else if (allSmurfs[i]._type == "PapaSmurf")
	{
		allSmurfs[i].feed(allSmurfs,i);
	}
	else
	{
		allSmurfs[i].feed(allSmurfs,i);
	}

	std::cout << "Yummy!!! " << allSmurfs[i]._name;
	std::cout << " tanks master " << allSmurfs[i]._masterName;
	std::cout << ". I have now " << allSmurfs[i]._lifePoints;
	std::cout << "living points." << std::endl;
}

/*
The function checks whether the selected dragons can perform magic
If she calls his phonia (by his type)
If it does not print an appropriate message
*/
void Game::fight()
{
	int i = 0;
	printAll();
	do {
		std::cout << "Choose a smurf to fight:" << std::endl;
		std::cin >> i;
	} while (i>allSmurfs.size() || i<0);

	if (allSmurfs[i]._lifePoints < 10||allSmurfs[i]._magicPoints<1)
	{
		std::cout << "The selected smurf does not have enough life. Try to feed or arm it" << std::endl;
	}
	else
	{
		if (allSmurfs[i]._type == "Smurfmelody")
		{
			allSmurfs[i].MakeMagic(allSmurfs, GargamelLives,i);
		}
		else if (allSmurfs[i]._type == "PapaSmurf")
		{
			allSmurfs[i].MakeMagic(allSmurfs, GargamelLives,i);
		}
		else
		{
			allSmurfs[i].MakeMagic(allSmurfs, GargamelLives, i);
		}

		/*std::cout << allSmurfs[i]._name;
		std::cout << "fought Gargamel, He now have ";
		std::cout << GargamelLives;
		std::cout << " points and ";
		std::cout << allSmurfs[i]._name << "have ";
		std::cout << allSmurfs[i]._lifePoints << "points";*/
	}
}


/*
The function prints all the gradients along with their details
(Not sorted for not repeating code snippets)
*/
void Game::printAll()
{
	std::cout << "The available smurfs are:" << std::endl;
	for (int i = 0;i < allSmurfs.size();i++)
	{
		std::cout << "[" << i << "]-" << allSmurfs[i]._type;
		std::cout << " - " << allSmurfs[i]._name << ",living points:";
		std::cout << allSmurfs[i]._lifePoints << ",Amount of Magics:" << allSmurfs[i]._magicPoints;
		std::cout << std::endl;
	}
}

/*
The function prints all the gradients along with their details 
and they are sorted by their type
*/
void Game::printList()
{
	std::cout << "Papa smurfs:" << std::endl;
	for (int i = 0;i < allSmurfs.size();i++)
	{
		if (allSmurfs[i]._type == "PapaSmurf")
		{
			std::cout << "Papa Smurf: " << allSmurfs[i]._name << ", living points:" << allSmurfs[i]._lifePoints << ", Amount of Magics:" << allSmurfs[i]._magicPoints << std::endl;
		}
	}

	std::cout << "Smurfmelodys:" << std::endl;
	for (int i = 0;i < allSmurfs.size();i++)
	{
		if (allSmurfs[i]._type == "Smurfmelodys")
		{
			std::cout << "Smurfmelodys: " << allSmurfs[i]._name << ", living points:" << allSmurfs[i]._lifePoints << ", Amount of Magics:" << allSmurfs[i]._magicPoints << std::endl;
		}
	}

	std::cout << "Brainy smurf:" << std::endl;
	for (int i = 0;i < allSmurfs.size();i++)
	{
		if (allSmurfs[i]._type == "BrainySmurf")
		{
			std::cout << "Brainy smurf: " << allSmurfs[i]._name << ", living points:" << allSmurfs[i]._lifePoints << ", Amount of Magics:" << allSmurfs[i]._magicPoints << std::endl;
		}
	}


}

/*
The function adds two magic points to the selected dredge
*/
void Game::GiveMoreAmount()
{
	int i = 0;
	printAll();
	do {
		std::cout << "Select the smurf you want to arm" << std::endl;
		std::cin >> i;
	} while (i>allSmurfs.size()||i<0);

	allSmurfs[i]._magicPoints += 2;
}