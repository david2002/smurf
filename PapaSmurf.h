#pragma once
#include"Smurfs.h"
#include<string>
#include <string.h>
#include <iostream>
#include<vector>

class PapaSmurf:public Smurfs
{
public:
	PapaSmurf(std::string Name, std::string Type);
	~PapaSmurf();
	virtual void MakeMagic(std::vector<Smurfs>& allSmurfs,int& GargamelLives, int i);
	virtual void feed(std::vector<Smurfs>& allSmurfs,int i);
};

